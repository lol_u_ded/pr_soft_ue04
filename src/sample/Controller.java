package sample;

import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import sample.model.GradeTableModel;
import sample.model.Results;
import sample.model.Student;

import java.lang.reflect.Field;


public class Controller {

    private final GradeTableModel model;
    private Stage primaryStage;

    @FXML
    private TableView<Results> resultTable;

    public Controller(){
        this.model = new GradeTableModel();
    }

    @FXML
    void exitProgram() {
        primaryStage.close();
    }

    @FXML
    void addStudent() {
        Stage popupStage = new Stage();
        popupStage.setTitle("Add new Student");
        popupStage.initModality(Modality.APPLICATION_MODAL);	// Todo From previous Tutor: should be in new controller with according fxml file -2
        popupStage.setHeight(250);
        popupStage.setWidth(400);


        TextField addId = new TextField();
        TextField addName = new TextField();
        TextField addFirstName = new TextField();
        ChoiceBox<Integer> skz= new ChoiceBox<>();
        skz.getItems().add(521);        // computer science
        skz.getItems().add(404);        // computer mathematics
        skz.getItems().add(321);        // computer science master
        skz.getItems().add(536);        // AI

        Button submitButton = new Button("Add");	// Todo From previous Tutor: no error message, when not all fields are filled -1
        submitButton.setOnAction(e1 -> {
            int sn = skz.getValue();
            String id = addId.getText();
            String firstName = addFirstName.getText();
            String name = addName.getText();
            model.data.add(new Results(new Student(id,firstName,name,sn)));
            popupStage.close();
        });

        Button cancel = new Button("Cancel");
        cancel.setOnAction(e2 -> popupStage.close());

        VBox vBox = new VBox();
        vBox.getChildren().add(new VBox(new Label("Name "), addName));
        vBox.getChildren().add(new VBox(new Label("First Name "), addFirstName));
        vBox.getChildren().add(new VBox(new Label("StudentID "), addId));
        vBox.getChildren().add(new VBox(new Label("SN "), skz));
        vBox.getChildren().add(submitButton);
        vBox.getChildren().add(cancel);

        popupStage.setScene(new Scene(vBox));
        popupStage.show();
    }

    @FXML
    void removeStudent() {
        Stage popupStage = new Stage();
        popupStage.setTitle("Delete selected Student?");
        popupStage.initModality(Modality.APPLICATION_MODAL);
        popupStage.setHeight(150);
        popupStage.setWidth(400);


        Button confirm = new Button("delete");
        confirm.setOnAction(e1 -> {
            resultTable.getItems().removeAll(resultTable.getSelectionModel().getSelectedItem());
            popupStage.close();
        });

        Button cancel = new Button("Cancel");
        cancel.setOnAction(e2 -> popupStage.close());

        VBox vBox = new VBox();

        vBox.getChildren().add(confirm);
        vBox.getChildren().add(cancel);

        popupStage.setScene(new Scene(vBox));
        popupStage.show();
    }



    public void setPrimaryStage(Stage primaryStage) {
        primaryStage.setResizable(false);
        this.primaryStage = primaryStage;
    }

    @FXML
    private void initialize() {
        resultTable.itemsProperty().setValue(model.data);
        resultTable.setEditable(true);
        resultTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        initializeTable();

        // initDB();

        //initializeResults();

    }



    // Todo From previous Tutor: TableColumns should be defined in fxml
    // Todo From previous Tutor: also the grade and sum are not updating
    private void initializeTable(){
        //Create Columns

        // Student ID
        TableColumn<Results, String> id = new TableColumn<>("ID");
        id.setCellValueFactory(new PropertyValueFactory<>("id"));
        resultTable.getColumns().add(id);

        // Student name
        TableColumn<Results, String> name = new TableColumn<>("Name");
        name.setCellValueFactory(new PropertyValueFactory<>("name"));
        resultTable.getColumns().add(name);

        // Student firstName
        TableColumn<Results, String> firstN = new TableColumn<>("First Name");
        firstN.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        resultTable.getColumns().add(firstN);

        // Student study number
        TableColumn<Results, String> sn = new TableColumn<>("SN");
        sn.setCellValueFactory(new PropertyValueFactory<>("sn"));
        resultTable.getColumns().add(sn);

        // Submissions
        String[] cols = getAssColumn();
        int i = 0;
        for (String col : cols) {
            TableColumn<Results, Integer> curr = new TableColumn<>(col);
            curr.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<Integer>(){
                @Override
                public String toString(Integer object) {
                    return object != null ? object.toString() : "";
                }

                @Override
                public Integer fromString(String s) {
                    try{
                        return Integer.parseInt(s);
                    } catch(NumberFormatException e) {
                        return null;
                    }
                }
            }));
            final int pos = i;
            curr.setCellValueFactory((TableColumn.CellDataFeatures<Results, Integer> param) -> {
                Results results = param.getValue();
                return results.assignmentsProperty()[pos].asObject();
            });
            resultTable.getColumns().add(curr);
            i++;


        }

        // Total points on Submissions
        TableColumn<Results, String> sum = new TableColumn<>("Sum");
        sum.setCellValueFactory(new PropertyValueFactory<>("sumPoints"));
        resultTable.getColumns().add(sum);

        // Grading
        TableColumn<Results, String> grade = new TableColumn<>("Grade");
        grade.setCellValueFactory(new PropertyValueFactory<>("grade"));
        resultTable.getColumns().add(grade);
    }

    private String[] getAssColumn() {
        Class<Results> resultsClass = Results.class;
        Field assField = null;
        int noAss = 9;              // default
        try {
            assField = resultsClass.getDeclaredField("NR_ASSIGNMENTS");
            assField.setAccessible(true);
            noAss = (int) assField.get(resultsClass);
        } catch (NoSuchFieldException | IllegalAccessException ignored) {
            // should not happen in that case it will use a DEFAULT value
        }
        String[] cols = new String[noAss];
        for (int i = 0; i < cols.length; i++) {
            cols[i] = 'A' + Integer.toString(i + 1);
        }
        return cols;
    }

    private void initializeResults(){
          model.data.addAll(DBManager.readAllResults());
    }

    private void initDB() {
        DBManager.openConnection(true);
        DBManager.createTables();

    }
}
