package sample.model;

import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableIntegerValue;

import java.util.Arrays;

public class Results {

	// Homework parameters
	public static final int NR_ASSIGNMENTS = 9;
	public static final int NR_VALID = 8;
	public static final int POINTS_VALID = 8;
	public static final int MIN_POINTS = 0;
	public static final int MAX_POINTS = 24;
	public static final int UNDEFINED = -1;

	// Grading percentages
	private static final double S_GUT = 87.5;
	private static final double GUT = 75.0;
	private static final double BEF = 62.5;
	private static final double GEN = 50.0;

	// Grading and sum property
	private IntegerProperty sum;
	private StringProperty grade;



	private final Student student;
	private final IntegerProperty[] assignments;
	
	public Results(Student student) {
		this.assignments = new IntegerProperty[NR_ASSIGNMENTS];
		Arrays.fill(assignments, new SimpleIntegerProperty(UNDEFINED));
		this.student = student;
		this.sum = createSumBinds();
		this.grade = createGradeBinds();
	}

	private StringProperty createGradeBinds() {
		SimpleStringProperty bind = new SimpleStringProperty("");
		bind.bind(Bindings.createStringBinding(() -> this.gradeProperty().toString()));
		return bind;
	}

	private IntegerProperty createSumBinds() {
		SimpleIntegerProperty bind = new SimpleIntegerProperty(0);
		bind.bind(Bindings.createIntegerBinding(() -> Arrays.stream(assignments)
				.map(ObservableIntegerValue::get)
				.reduce(0,(a,b) ->  a > UNDEFINED && b > UNDEFINED ? a = (a + b) : a),assignments)
		);
		return bind;
	}

	//Properties
	public StringProperty idProperty() { return student.idProperty(); }

	public StringProperty firstNameProperty() {
		return student.firstNameProperty();
	}

	public StringProperty nameProperty() {
		return student.nameProperty();
	}

	public IntegerProperty snProperty() {
		return student.snProperty();
	}

	public IntegerProperty[] assignmentsProperty() {
		return assignments;
	}

	public Student studentProperty(){
		return student;
	}

	public IntegerProperty ptsProperty(int idx){
		return assignments[idx];
	}

	//set
	public void setPoints(int idx, int ps) {
		if(idx < 0 || idx > NR_ASSIGNMENTS){
			// no action ErrorCase idx out of Bounce
			return;
		}
		if(ps < MIN_POINTS || ps > MAX_POINTS){
			// no action ErrorCase point value undefined
			return;
		}
		assignments[idx] = new SimpleIntegerProperty(ps);
	}

	// Working Methods

	// Check if the student meets the requirement for positive grading
	public boolean isPositive(){
		int negAss = 0;
		for (IntegerProperty assignment : assignments) {
			if (assignment.get() < POINTS_VALID) {
				negAss++;
			}
		}
		return (NR_ASSIGNMENTS - negAss) >= NR_VALID;
	}

	//get sum of points & ignore UNDEFINED
	public IntegerProperty sumPointsProperty(){
		int sum = 0;
		for(IntegerProperty curr : assignments){
			if(curr.get() != UNDEFINED){
				sum += curr.get();
			}
		}
		return new SimpleIntegerProperty(sum);
	}


	//check if there are too many Undefined
	public boolean isDone(){
		int undef = 0;
		for (IntegerProperty assignment : assignments) {
			if (assignment.get() == UNDEFINED) {
				undef++;
			}
		}
		int border = (NR_ASSIGNMENTS - NR_VALID);
		return undef <= border;
	}

//	// Returns Grading in form of string
//	public String getGrade(){
//
//		if(!isDone()){
//			return "-";
//		}
//		if(!isPositive()){
//			return"Nicht Genügend";
//		}
//
//		double maxPts = MAX_POINTS*NR_ASSIGNMENTS;
//		double percent = (sumPointsProperty().get()*100) / maxPts;
//
//		if(percent >= S_GUT){
//			return "Sehr Gut";
//		}else if(percent >= GUT){
//			return "Gut";
//		}else if(percent >= BEF){
//			return "Befriedigend";
//		}else if(percent >= GEN){
//			return "Genügend";
//		}else{
//			return"Nicht Genügend";
//		}
//	}

	// Returns Grading in form of stringProperty
	public StringProperty gradeProperty(){

		if(!isDone()){
			return new SimpleStringProperty("-");
		}
		if(!isPositive()){
			return new SimpleStringProperty("Nicht Genügend");
		}

		double maxPts = MAX_POINTS*NR_ASSIGNMENTS;
		double percent = (sumPointsProperty().get()*100) / maxPts;

		if(percent >= S_GUT){
			return new SimpleStringProperty("Sehr Gut");
		}else if(percent >= GUT){
			return new SimpleStringProperty("Gut") ;
		}else if(percent >= BEF){
			return new SimpleStringProperty("Befriedigend");
		}else if(percent >= GEN){
			return new SimpleStringProperty("Genügend");
		}else{
			return new SimpleStringProperty("Nicht Genügend");
		}
	}
}
