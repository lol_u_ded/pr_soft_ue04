package sample.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class GradeTableModel {
    public ObservableList<Results> data = FXCollections.observableList(TestData.createData());
}
