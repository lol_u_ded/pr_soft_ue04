package sample.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;

public class TestData {

	public static ObservableList<Results> createData() {
		ObservableList<Results> data = FXCollections.observableArrayList();
		
		Results r1 = new Results(new Student("k01245678", "Mia", "Musterstudentin", 521));
		r1.setPoints(0, 24);
		r1.setPoints(1, 24);
		r1.setPoints(2, 24);
		r1.setPoints(3, 24);
		r1.setPoints(4, 24);
		r1.setPoints(5, 24);
		r1.setPoints(6, 24);
		r1.setPoints(7, 24);
		r1.setPoints(8, 23);
		data.add(r1);
		
		Results r2 = new Results(new Student("k01778901", "Max", "Mustermann", 521));
		r2.setPoints(0, 23);
		r2.setPoints(1, 19);
		r2.setPoints(2, 24);
		r2.setPoints(3, 17);
		r2.setPoints(4, 18);
		r2.setPoints(5, 21);
		r2.setPoints(6, 21);
		r2.setPoints(7, 18);
		r2.setPoints(8, 20);
		data.add(r2);
		
		Results r3 = new Results(new Student("k01612345", "Fred", "Faul", 521));
		r3.setPoints(0, 4);
		r3.setPoints(1, 12);
		r3.setPoints(2, 13);
		r3.setPoints(3, 9);
		r3.setPoints(4, 0);
		r3.setPoints(5, 11);
		r3.setPoints(6, 8);
		r3.setPoints(7, 14);
		r3.setPoints(8, 11);
		data.add(r3);
		
		Results r4 = new Results(new Student("k01612345", "Ulrike", "Unfertig", 521));
		r4.setPoints(0, 18);
		r4.setPoints(1, -1);
		r4.setPoints(2, -1);
		r4.setPoints(3, -1);
		r4.setPoints(4, -1);
		r4.setPoints(5, -1);
		r4.setPoints(6, -1);
		r4.setPoints(7, -1);
		r4.setPoints(8, -1);
		data.add(r4);


		Results r5 = new Results(new Student("k01687145", "Heidi", "Klum", 321));
		r5.setPoints(0, 10);
		r5.setPoints(1, 11);
		r5.setPoints(2, 12);
		r5.setPoints(3, 13);
		r5.setPoints(4, 14);
		r5.setPoints(5, 15);
		r5.setPoints(6, 16);
		r5.setPoints(7, 17);
		r5.setPoints(8, -1);
		data.add(r5);


		Results r6 = new Results(new Student("k08212345", "Kurt", "Knapp", 521));
		r6.setPoints(0, 18);
		r6.setPoints(1, 18);
		r6.setPoints(2, 16);
		r6.setPoints(3, 17);
		r6.setPoints(4, 13);
		r6.setPoints(5, 15);
		r6.setPoints(6, 19);
		r6.setPoints(7, 14);
		r6.setPoints(8, 10);
		data.add(r6);


		Results r7 = new Results(new Student("k01233216", "David", "Durchgefallen", 521));
		r7.setPoints(0, 18);
		r7.setPoints(1, 5);
		r7.setPoints(2, 14);
		r7.setPoints(3, 24);
		r7.setPoints(4, 20);
		r7.setPoints(5, 19);
		r7.setPoints(6, -1);
		r7.setPoints(7, 15);
		r7.setPoints(8, 19);
		data.add(r7);


		Results r8 = new Results(new Student("k11904991", "Rudolf", "Garstenauer", 521));
		r8.setPoints(0, 23);
		r8.setPoints(1, 21);
		r8.setPoints(2, 23);
		r8.setPoints(3, 22);
		r8.setPoints(4, 18);
		r8.setPoints(5, 20);
		r8.setPoints(6, 24);
		r8.setPoints(7, 23);
		r8.setPoints(8, 19);
		data.add(r8);
		
		return data;
	}
}
