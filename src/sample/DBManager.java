package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import sample.model.Results;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class DBManager {

    // DB QUERY STRINGS
    private static final String DB_URL = "jdbc:derby:D:/OneDrive/Informatik/Semester 4/PR_Soft 2/UE04/JavaFX_HWII/testDB;create=true";
    private static final String TABLE_NAME = "GradeTable";
    private static final String STUDENT_ID = "ID";
    private static final String STUDENT_FIRST_NAME = "First Name";
    private static final String STUDENT_LAST_NAME = "Name";
    private static final String STUDENT_SN = "SN";
    private static final String[] STUDENT_ASSIGNMENT_X = {"A1","A2","A3","A4","A5","A6","A7","A8","A9"};

    private static final String READ_ALL_DATA = "SELECT * FROM " + TABLE_NAME;

    private static final String UPDATE_ASSIGNMENT_POINTS =
            "UPDATE " + TABLE_NAME +
                    " SET " + Arrays.toString(STUDENT_ASSIGNMENT_X) + "=?, " +
                    "WHERE " + STUDENT_ID + "=?";

    private static final String INSERT_STUDENT =
            "INSERT INTO " + TABLE_NAME +
                    "(" + STUDENT_ID + ", " + STUDENT_LAST_NAME + ", " + STUDENT_FIRST_NAME + ", " + STUDENT_SN +
                    ") VALUES (" + "=?, " + "=?, " + "=?, " + "=?, " + ")";


    // DBManager fields
    private static Connection dbConnection;

    public DBManager() {
        dbConnection = null;
    }

    // ---------------------------------------------------------------

    // Basic Methods

    // This Method should open the DB-connection
    public static void openConnection(boolean newDb) {
        try {
            dbConnection = DriverManager.getConnection(DB_URL);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                assert dbConnection != null;
                dbConnection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }if(newDb) {
            deleteTables();
            createTables();
        }
    }

    // Create all Tables that are needed for the exercise
    public static void createTables() {
        try (Statement statement = dbConnection.createStatement()) {
            statement.execute(String.format("CREATE TABLE %s ("
                            + "%s VARCHAR PRIMARY KEY "
                            + "%s VARCHAR(30), "
                            + "%s VARCHAR(15), "
                            + "%s INT, "
                            + "%s INT, "    //a1
                            + "%s INT, "    //a2
                            + "%s INT, "    //a3
                            + "%s INT, "    //a4
                            + "%s INT, "    //a5
                            + "%s INT, "    //a6
                            + "%s INT, "    //a7
                            + "%s INT, "    //a8
                            + "%s INT, ",   //a9
                    TABLE_NAME, STUDENT_ID, STUDENT_LAST_NAME, STUDENT_FIRST_NAME, STUDENT_SN,
                    STUDENT_ASSIGNMENT_X[0],STUDENT_ASSIGNMENT_X[1],STUDENT_ASSIGNMENT_X[2],STUDENT_ASSIGNMENT_X[3]
                    ,STUDENT_ASSIGNMENT_X[4],STUDENT_ASSIGNMENT_X[5],STUDENT_ASSIGNMENT_X[6],STUDENT_ASSIGNMENT_X[7]
                    ,STUDENT_ASSIGNMENT_X[8]));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // simply drop the table
    private static void deleteTables() {
        try (Statement statement = dbConnection.createStatement()) {
            statement.execute(String.format("DROP TABLE %s", TABLE_NAME));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // exit the current connection
    private static void closeConnection() {
        if (dbConnection == null) {
            throw new IllegalStateException("Connection was already closed");
        }

        try {
            dbConnection.close();
        } catch (SQLException e) {
            throw new RuntimeException("Could not close database", e);
        }
    }

    // ---------------------------------------------------------------

    // read in all Results to the DB
    public static List<Results> readAllResults() {

        checkConnection();

        List<Results> resultsList = new ArrayList<>();

        try {
            PreparedStatement statement = dbConnection.prepareStatement(READ_ALL_DATA);

            ResultSet resultSet = statement.executeQuery();

            int idx = 0; // todo update
            while(resultSet.next()){

                Results results = (Results) resultSet.getObject(idx);

                // Listener for total Pts
                final int[] i = {0};
                Arrays.stream(results.assignmentsProperty()).forEach(s -> s.addListener(new ChangeListener<Number>() {
                    @Override
                    public void changed(ObservableValue<? extends Number> observableValue, Number oldValue, Number newValue) {
                        String curr = "A"+ i[0];
                        i[0]++;
                        updatePTS(curr,i[0],newValue, results.ptsProperty(i[0]).toString());
                    }
                }));
            }

        } catch (SQLException e) {
            throw new RuntimeException("Unable to read products");
        }

        return resultsList;
    }

    // update the PTS value of a particular assignment & student
    private static void updatePTS(String studentAssignment,int idx, Number newValue, String oldValue) {

        checkConnection();

        try {
            PreparedStatement statement = dbConnection.prepareStatement(UPDATE_ASSIGNMENT_POINTS,Statement.RETURN_GENERATED_KEYS);

            statement.setInt(idx,newValue.intValue());
            statement.execute();

        } catch (SQLException e) {
            System.out.printf("Failed to execute Update %s to %s",studentAssignment, newValue);
        }

    }

    // insert a new Student
    private static void insertStudent(String id, String name, String firstName, int sn){

        checkConnection();

        if(isDuplicateStudent(id)){
            System.out.printf("Student with id %s already exists!\n", id);
            return;
        }

        try {
            PreparedStatement statement = dbConnection.prepareStatement(INSERT_STUDENT);
            {
                statement.setString(1, id);
                statement.setString(2, name);
                statement.setString(3, firstName);
                statement.setInt(4, sn);
                statement.execute();
            }
        } catch (SQLException e) {
            System.out.println("Wasn't able to execute insert statement");
        }
    }

    // ---------------------------------------------------------------

    // Checking Methods

    // checks if connection is still there
    private static void checkConnection () {
        if(dbConnection == null) {
            throw new IllegalStateException("No existing connection available.");
        }
    }

    // checks if there is already a student with that ID
    private static boolean isDuplicateStudent(String id) {
        try {
            Statement statement = dbConnection.createStatement();
            ResultSet s = statement.executeQuery(String.format("SELECT %s FROM %s WHERE %s = %s",STUDENT_ID, TABLE_NAME, STUDENT_ID, id));

            int i = 0;
            while(s.next()){
                if(s.getString(i).equals(id.toString())){
                    return true;
                }
            }
            return false;

        } catch (SQLException e) {
            System.out.println("Wasn't able to check the Student_ID!");
        }

        return true;
    }

    // ---------------------------------------------------------------
}
